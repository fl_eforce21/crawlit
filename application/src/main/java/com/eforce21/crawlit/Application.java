package com.eforce21.crawlit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Florian Lindner <florian@f-lindner.de>
 */
//Pfade unterprojekte gleich bis crawlit -> kein ComponentScan nötig
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }
}
