package com.eforce21.crawlit.socialmedia;


import com.eforce21.crawlit.dto.SocialMediaDto;

/**
 * @author Florian Lindner <florian@f-lindner.de>
 */
public interface SocialMediaService {

    SocialMediaDto getInfoForName(String name);

    SocialMediaDto getInfoForFullName(String name, String sureName);
}
