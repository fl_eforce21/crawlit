package com.eforce21.crawlit.socialmedia;

import com.eforce21.crawlit.dto.SocialMediaDto;
import org.springframework.stereotype.Service;

/**
 * @author Florian Lindner <florian@f-lindner.de>
 */
@Service
public class SocialMediaServiceImpl implements SocialMediaService {

    @Override
    public SocialMediaDto getInfoForName(String name) {
        // Geht an facebook adapter und holt daten
        // Geht an Linkedin dapter
        // speichert in datenbank mit persistence adapter
        // validiert daten
        // gibt daten validiert zurück
        return new SocialMediaDto(){{
            setName(name);
        }};
    }

    @Override
    public SocialMediaDto getInfoForFullName(String name, String sureName) {
        return null;
    }
}
