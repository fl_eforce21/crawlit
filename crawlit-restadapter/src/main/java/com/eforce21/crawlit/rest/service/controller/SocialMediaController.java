package com.eforce21.crawlit.rest.service.controller;

import com.eforce21.crawlit.dto.SocialMediaDto;
import com.eforce21.crawlit.socialmedia.SocialMediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author Florian Lindner <florian@f-lindner.de>
 */
@RestController
@RequestMapping("/api")
public class SocialMediaController  {

    @Autowired
    private SocialMediaService socialMediaService;

    @RequestMapping(value = "/info/{name}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public SocialMediaDto getInfoForName(@PathVariable("name") String name) {
        return this.socialMediaService.getInfoForName(name);
        //TODO Hier crawlit-api-impl aufrufen
    }

    public SocialMediaDto getInfoForFullName(String name, String sureName) {
        return null;
    }
}
